chocolatey feature enable -n=allowGlobalConfirmation

choco install git --force
choco install 7zip --force
choco install dotnetcore --force
choco install dotnetcore-sdk --force

chocolatey feature disable -n=allowGlobalConfirmation

reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce /d $env:USERPROFILE\Desktop\shell\InstallBuildEnv-Phase2.bat;

Restart-Computer -f
