$projectDir = "${env:systemdrive}\projects"

If (!(Test-Path $projectDir)) {
    New-Item -Path $projectDir -ItemType Directory
}

cd $projectDir

git clone https://github.com/Teletrax/CIAssignment

cd CIAssignment

dotnet restore

dotnet build .\src\FourC.Worker.Api

dotnet build .\src\FourC.Worker.Backend
